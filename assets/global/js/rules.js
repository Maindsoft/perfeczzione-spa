/**
 * reglas.js
 * Autor: Luis Cristerna (Maindsoft)
 * Fecha: 14/09/2018
 */

/**
 * Metodo para validar un campo vacio
 * @param campo
 * @param nombre_campo
 */
function is_campo_vacio(campo, nombre_campo)
{
    if (campo === '')
    {
        alert("Por favor ingresa tu " + nombre_campo);
        return false;
    }else {
        return true;
    }
}

function is_campo_corto(campo, nombre_campo, tamano_campo) {
    if (campo.length <= tamano_campo)
    {
        longitud_palabra = campo.length;
        alert("El campo " + nombre_campo + " es demasiado corto")
        return false
    }
}
function is_descripcion_seo(campo, nombre_campo)
{
    if (campo.length <= 129){
        longitud_palabra = campo.length;
        alert("La " + nombre_campo +" solo tiene "  + longitud_palabra + " caracteres, lo ideal para el SEO es entre 130 y 155");
        return false;
    }
}

function is_archivo_vacio(campo, nombre_campo)
{
    if ('files' in campo){
        if (campo.files.length == 0){
            alert("Te falta subir un " + nombre_campo);
            return false;
        }else {
            return true;
        }
    }
}

function is_archivo_valido(id_input) {
    var archivo = document.getElementById(id_input);
    var ruta_archivo = archivo.value;
    var extensiones_permitidas = /(.pdf|.jpg|.jpeg|.png|.doc|.docx)$/i;
    
    if (!extensiones_permitidas.exec(ruta_archivo)){
        alert("Ingresa un tipo de archivo válido");
        return false;
    }else {
        return true;
    }
}

function is_imagen_valida(id_input) {
    var archivo = document.getElementById(id_input);
    var ruta_archivo = archivo.value;
    var extensiones_permitidas = /(.ico|.png|.jpg|.jpeg)$/i;

    if (!extensiones_permitidas.exec(ruta_archivo)){
        alert("Ingresa un tipo de imagen válida");
        return false;
    }else {
        return true;
    }
}

/**
 * Metodo para verificar que el campo es un telefono valido
 * @param campo
 * @param nombre_campo
 */
function is_telefono(campo, nombre_campo)
{
    if( !(/^\d{10}$/.test(campo)) ) {
        alert("Por favor ingresa un " + nombre_campo + " " + "valido");
        return false;
    }else {
        return true;
    }
}

/**
 * Metodo para verificar que el campo es un correo electronico valido
 * @param campo
 * @param nombre_campo
 */
function is_email(campo, nombre_campo)
{
    var patron = /\S+@\S+/;
    if (!patron.test(campo))
    {
        alert("Por favor ingresa un " + nombre_campo + " " + "valido");
        return false;
    }else {
        return true;
    }
}
